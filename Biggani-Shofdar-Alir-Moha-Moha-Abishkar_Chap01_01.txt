কাচ্চি বিরিয়ানি

বিজ্ঞানী সফদর আলীর সাথে আমার পরিচয় জিলিপি খেতে গিয়ে। আমার জিলিপি খেতে খুব ভালো লাগে, গরম গরম মুচমুচে জিলিপি থেকে খেতে ভালো আর কী আছে? আমি তাই সময় পেলেই কাওরান বাজারের কাছে একটা দোকানে জিলিপি খেতে আসি। আজকাল আর কিছু বলতে হয় না, আমাকে দেখলেই দোকানি একগাল হেসে বলে, ‘বসেন স্যার। আর দশ মিনিট।’ অর্থাৎ জিলিপি ভাজা শেষ হতে আর দশ মিনিট। দোকানের বাচ্চা ছেলেটা খবরের কাগজটা দিয়ে যায়, আমি বসে খুঁটিয়ে খুঁটিয়ে পড়ি, খবরগুলো পড়ে বিজ্ঞাপনে চোখ বোলাতে বোলাতে জিলিপি এসে যায়। খেয়ে আমাকে বলতে হয় কেমন হয়েছে, ভালো না হলে নাকি পয়সা দিতে হবে না।

	সেদিনও তেমনি বসেছি জিলিপি খেতে, প্লেটে গরম জিলিপি থেকে ধোঁয়া উঠছে, আমি সাবধানে একটা নিয়ে কামড় দেবার চেষ্টা করছি, এত গরম যে একটু অসাবধান হলেই জিভ পুড়ে যাবে! তখন আমার সামনে আরেকজন এসে বসলেন; ছোটখাটো শুকনা মতন চেহারা, মাথায় এলোমেলো চুল; মুখে বেমানান বড় বড় গোঁফ। চোখে ভারী চশমা দেখে কেমন যেন রাগী রাগী মনে হয়। শার্টের পকেটটি অনেক বড়, নানারকম জিনিসে সেটি ভর্তি, মনে হল সেখানে জ্যান্ত জিনিসও কিছু একটা আছে, কারণ কিছু একটা যেন সেখানে নড়াচড়া করছে! ভদ্রলোকটিও জিলিপি অর্ডার দিলেন, আমার মতন নিশ্চয়ই জিলিপি খেতে পছন্দ করেন। জিলিপি আসার সাথে সাথে তিনি পকেট থেকে টর্চ লাইটের মতো একটা জিনিস বের করলেন, সেটার সামনে কয়েকটি সুঁচালো কাঁটা বের হয়ে আছে। জিনিসটি দিয়ে তিনি একটা জিলিপি গেঁথে ফেলে কোথায় যেন একটা সুইচ টিপে দেন, সাথে সাথে ভিতরে একটা পাখা শোঁ শোঁ করে ঘুরতে থাকে, ভিতর থেকে জোর বাতাস বের হয়ে আসে। দশ সেকেন্ডে জিলিপি ঠাণ্ডা হয়ে আসে, সাথে সাথে তিনি জিলিপিটা মুখে পুরে দিয়ে তৃপ্তি করে চিবুতে থাকেন। আমি অবাক হয়ে পুরো ব্যাপারটি লক্ষ করছিলাম; আমার চোখে চোখ পড়তেই ভদ্রলোক একটু লাজুকভাবে হেসে বললেন, ‘জিলিপি খেতে গিয়ে সময় নষ্ট করে কি লাভ?’

	সত্যি তিনি সময় নষ্ট করলেন না, আমি দুটো জিলিপি খেয়ে শেষ করতে-করতে তাঁর পুরো প্লেট শেষ। সময় নিয়ে তাঁর সত্যি সমস্যা রয়েছে; কারণ হঠাৎ তাঁর শার্টের কোনো একটা পকেট থেকে একটা অ্যালার্ম বেজে ওঠে, সাথে সাথে তিনি লাফিয়ে উঠে পানি না খেয়েই প্রায় ছুটে বের হয়ে গেলেন। যাবার সময় পয়সা পর্যন্ত দিলেন না, হাত নেড়ে দোকানিকে কী একটা বলে গেলেন, দোকানিও মাথা নেড়ে খাতা বের করে কী একটা লিখে রাখল।

	আমি বের হবার সময় দোকানিকে জিজ্ঞেস করলাম, লোকটা কে। দোকানি নিজেও ভালো করে চেনে না, নাম সফদর আলী। মাসিক বন্দোবস্ত করা আছে, বৃহস্পতিবার করে নাকি জিলিপি খেতে আসেন। মাথায় টোকা দিয়ে দোকানি বলল, ‘মাথায় একটু ছিট আছে।’

	ছিট জিনিসটা কী আমি জানি না। কিন্তু মাথায় সেটি থাকলে লোকগুলো একটু অন্যরকম হয়ে যায়; আর ঠিক এই ধরনের লোকই আমার খুব পছন্দ! কে এক সাধু নাকি দশ বছর থেকে হাত উপরে তুলে আছে শুনে আমি পকেটের পয়সা খরচ করে সীতাকুণ্ড গিয়েছিলাম। যখনই আমি খবর পাই কোনো পীর হাতের ছোঁয়ায় এক টাকার নোটকে এক শ’ টাকা বানিয়ে ফেলছে, আমি সেটা নিজের চোখে দেখে আসার চেষ্টা করি। এত দিন হয়ে গেল এখনো একটা সত্যিকার পীরের দেখা পাই নি, সবাই ভণ্ড। আমার অবশ্যি তাতে কোনো ক্ষতি হয় নি, মজার মানুষ দেখা আমার উদ্দেশ্য, ভণ্ড পীরের মতো মজার মানুষ আর কে আছে? সফদর আলী যদিও পীর নন। কিন্তু একটা মজার মানুষ তো বটেই! তাই সফদর আলীকে আবার দেখার জন্যে পরের বৃহস্পতিবার আমি আবার সেই দোকানে গিয়ে একই টেবিলে বসে অপেক্ষা করতে থাকি।

	ঠিকই সফদর আলী সময়মতো হাজির। বাইরে বৃষ্টি পড়ছিল। তাই ভিজে চুপচুপে হয়ে গেছেন। কিন্তু তাঁর দিকে তাকিয়ে আমি চমকে উঠি, চশমার কাচের উপর গাড়ির ওয়াইপারের মতো ছোট ছোট দু’টি ওয়াইপার শাঁই শাঁই করে পানি পরিষ্কার করছে। দোকানের ভিতরে ঢুকে কোথায় কী একটা সুইচ টিপে দিতেই ওয়াইপার দু’টি থেমে গিয়ে উপরে আটকে গেল। সফদর আলী আমার দিকে তাকিয়ে একটু লাজুকভাবে হেসে কৈফিয়ত দেয়ার ভঙ্গিতে বললেন, ‘বৃষ্টিতে চশমা ভিজে গেলে কিছু দেখা যায় না কিনা!’

	আমি জিজ্ঞেস না করে পারলাম না, ‘কোথায় পেলেন এরকম চশমা?’

	‘পাব আর কোথায়, নিজে তৈরি করে নিয়েছি।’

	আমি তখনই প্রথম জানতে পারলাম, সফদর আলী আসলে একজন শখের বিজ্ঞানী। কথা একটু কম বলেন, একটু লাজুক গোছের মানুষ, কিন্তু মজার মানুষ তো বটেই! আমি তাঁর কাপড়ের দিকে লক্ষ করে বললাম, ‘একেবারে তো ভিজে গেছেন, ঠাণ্ডা না লেগে যায়।’

	‘না, ঠাণ্ডা লাগবে না,’ বলে পকেটে আরেকটা কী সুইচ টিপে দিলেন। একটু পরেই তাঁর কাপড়-জামা থেকে পানি বাষ্পীভূত হয়ে উড়ে যেতে থাকে। কিছুক্ষণের মাঝে কাপড় একেবারে শুকনো খটখটে। সফদর আলী পকেটে হাত ঢুকিয়ে কী-একটা টিপে আবার সুইচ বন্ধ করে দিলেন। আমার বিস্মিত ভাবভঙ্গি দেখে আবার কৈফিয়ত দেয়ার ভঙ্গিতে বললেন, ‘কাপড়ের সুতার মাঝে মাঝে নাইক্রোম ঢুকিয়ে দিয়েছি, যখন খুশি গরম করে নেয়া যায়। পকেটে ব্যাটারি আছে।’

	সফদর আলী আবার জিলিপি অর্ডার দিলেন। আজকেও টর্চ লাইটের মতো দেখতে সেই ঠাণ্ডা করার যন্ত্রটা ব্যবহার করেছেন। কিন্তু বোঝা যাচ্ছে খুব তাড়াহুড়া নেই। আমি তাই আলাপ জমানোর চেষ্টা করলাম, ‘প্রায়ই আসেন বুঝি এখানে?’

	‘না, শুধু বৃহস্পতিবার। বৃহস্পতিবার ছুটি কিনা!’

	কোনো অফিস বৃহস্পতিবার ছুটি হয় জানতাম না, তাই একটু অবাক হয়ে জিজ্ঞেস করলাম, ‘কী অফিস এটা যে বৃহস্পতিবারে আপনার ছুটি?’

	‘না না, আমার ছুটি নয়! বৃহস্পতিবার আমি ছুটি দিই।’

	‘কাদের ছুটি দেন?’

	‘এই, আমার একধরনের ছাত্রদের বলতে পারেন।’ সফদর আলী একটু আমতা আমতা করে থেমে গেলেন, ঠিক বলতে চাইছেন না, তাই আমি আর তাঁকে ঘাঁটালাম না।

	সফদর আলী খুব মিশুক নন, তবে কথাবার্তা বলেন। অনেকক্ষণ ধরে তাঁর সাথে কথা হল। অনেক কিছু জানেন আর মাথায় অনেক ধরনের পরিকল্পনা, তাই তাঁর কথা শুনতেই ভারি মজা! ব্যাঙের ছাতার চাষ করে কীভাবে খাদ্য সমস্যা মেটানো যায় বা কেঁচো পুষে কীভাবে ঘরের আবর্জনা দূর করা যায়, সে থেকে শুরু করে সংখ্যা কেন দশভিত্তিক না হয়ে ষোলভিত্তিক হওয়া দরকার—এধরনের ব্যাপারে তাঁর উৎসাহ। সংখ্যা ষোলভিত্তিক হলে কম্পিউটার দিয়ে কাজ করা নাকি খুব সহজ হবে, আজকাল সব মাইক্রোপ্রসেসর নাকি ষোল কিংবা বত্ৰিশ বিটের হয়, সেটার মানে কী, আমি জানি না। সফদর আলী বলেছেন, আমাকে আরেক দিন বুঝিয়ে দেবেন। তিনি নিজে সবসময়েই ষোলভিত্তিক সংখ্যায় হিসেব করেন, তাই তিনি গোনেন খুব অদ্ভুতভাবে। সাত, আট, নয়ের পর দশ না বলে বলেন কুরা। তারপর কিলি, চিংগা, পিরু, মিকা, ফিকার পরে নাকি আসে দশ! আর সবচেয়ে আশ্চর্য ব্যাপার হচ্ছে, তিনি যখন দশ বলেন তার অর্থ নাকি ষোল! তিনি যখন বলেন এক শ’, তার অর্থ নাকি দুই শ’ ছাপ্পান্ন! ব্যাপারটি যে ফাজলামি নয় সেটা আমি জেনেছি বিশ্ববিদ্যালয়ের অঙ্কের প্রফেসরের সাথে কথা বলে, সত্যি নাকি এ ধরনের সংখ্যা হওয়া সম্ভব!আর সবচেয়ে আশ্চর্য ব্যাপার হচ্ছে, তিনি যখন দশ বলেন তার অর্থ নাকি ষোল! তিনি যখন বলেন এক শ’, তার অর্থ নাকি দুই শ’ ছাপ্পান্ন! ব্যাপারটি যে ফাজলামি নয় সেটা আমি জেনেছি বিশ্ববিদ্যালয়ের অঙ্কের প্রফেসরের সাথে কথা বলে, সত্যি নাকি এ ধরনের সংখ্যা হওয়া সম্ভব!

	পরের বৃহস্পতিবার আবার সফদর আলীর সাথে দেখা। তাঁকে একটু চিন্তিত দেখা গেল। গিনিপিগের লোমে তেল-হলুদ লেগে গেলে সেটা কীভাবে পরিষ্কার করা যায় বা লোম পুড়ে গেলে পোড়া গন্ধটা দূর করা যায় কীভাবে জানি কি-না জিজ্ঞেস করলেন। খুব সঙ্গত কারণেই আমি সেটা জানতাম না, গিনিপিগের লোমে তেল-হলুদ লাগতে পারে কীভাবে কিংবা পুড়ে যেতে পারে কী ভাবে, সেটা কিছুতেই আমার মাথায় এল না। তিনি কি শেষ পর্যন্ত গিনিপিগ রান্না করে খাওয়া শুরু করেছেন নাকি?

	আজ কথাবার্তা খুব বেশি জমল না। কী যেন চিন্তা করে একটা কাগজে অনেকগুলো দাগ টেনে একটা দাগ আরেকটার সাথে জুড়ে দিয়ে গভীর মনোযোগ দিয়ে কী যেন দেখতে লাগলেন। আমি তাঁকে আর বিরক্ত করলাম না, আমার নিজেরও কাজ ছিল, তাই উঠে পড়ার উদ্যোগ করতেই সফদর আলী জিজ্ঞেস করলেন, ‘আপনি বিরিয়ানি রান্না করতে পারেন?’

	‘আমি? বিরিয়ানি?’ আমি মাথা নেড়ে জানালাম, ‘রাঁধতে পারি না, শুধু খেতে পারি।’

	সফদর আলীর মুখটা একটু বিমর্ষ হয়ে গেল দেখে বললাম, ‘আমার বোন থাকে শান্তিনগরে, সে খুব ভালো কাচ্চি বিরিয়ানি রাঁধতে পারে।’

	সফদর আলী খুব আগ্রহ নিয়ে জিজ্ঞেস করলেন, ‘আপনার বোনকে বলবেন একটা কাগজে লিখে দিতে? বুঝলেন কিনা, করব যখন ভালো করেই করি!’

	‘কী করবেন?’

	সফদর আলী আমতা আমতা করে প্রশ্নটা এড়িয়ে গেলেন, তাই আমি আর কিছু বললাম না। জিজ্ঞেস করলাম, ‘কবে দরকার আপনার বিরিয়ানির রেসিপি?’

	‘কাল দিতে পারবেন?’

	‘কোথায় দেব?’

	‘এইখানে।’

	আমার তাঁর বাসাটা দেখার ইচ্ছে, তাই বললাম, ‘আপনার বাসাতে নিয়ে আসতে পারি, আমার কোনো অসুবিধে নেই।’

	সফদর আলী একটু ইতস্তত করে রাজি হলেন। একটা কাগজে ঠিকানাটা লিখে দিলেন, শ্যামলীর কাছে কোথায় জানি থাকেন।

	পরদিন আমি ঠিকানা খুঁজে সফদর আলীর বাসা বের করলাম। একটু নির্জন এলাকায় বেশ বড় একটা একতলা বাসা। দরজায় বেল টিপতেই ঘেউ-ঘেউ করে একটা কুকুর ভয়ানক রাগী গলায় চিৎকার করতে থাকে। কুকুরকে আমার খুব ভয় করে, আমি তাড়াতাড়ি দুই পা পিছিয়ে আসি। সফদর আলী দরজা একটু ফাঁক করে নিজের মাথাটা বের করে জিজ্ঞেস করলেন, ‘এনেছেন?’

	আমি ভেবেছিলাম আমাকে হয়তো ভিতরে বসতে বলবেন, কিন্তু তার সেরকম আমি ভেবেছিলাম আমাকে হয়তো ভিতরে বসতে বলবেন, কিন্তু তার সেরকম ইচ্ছে আছে বলে মনে হল না। বদরাগী কুকুরটার ডাক শুনে আমার নিজের ইচ্ছেও কমে এসেছে, তাঁর হাতে কাগজটা দিয়ে চলে এলাম। আসার আগে দরজার ফাঁক দিয়ে একটু উঁকি মারার চেষ্টা করে মনে হল মেঝেতে ইঁদুরের মতো অনেকগুলো কী যেন ঘোরাঘুরি করছে। গিনিপিগের কথা বলছিলেন, তাই হবে হয়তো।

	পরের বৃহস্পতিবার সফদর আলীকে খুব খুশি খুশি দেখা গেল। নিজে থেকে আলাপ শুরু করলেন, বললেন, ‘বুঝলেন ইকবাল সাহেব, আমার কাজ প্রায় শেষ। এখন লবণ একটু কম হয়, কিন্তু এমনিতে ফার্স্ট ক্লাস জিনিস!’

	‘কিসে লবণ কম হয়?’

	‘কেন, বিরিয়ানিতে! মনে নেই, আপনি রেসিপি এনে দিলেন?’

	‘ও!’ আমি একটু অবাক হয়ে জিজ্ঞেস করলাম, ‘আপনি নিজেই রাঁধছেন বুঝি?’

	‘মাথা খারাপ আপনার, আমি রাঁধব? রান্না করা আমার দু’চোখের বিষ!’

	‘তাহলে কি ভালো বাবুর্চি পেয়েছেন নাকি?’

	সফদর আলী হা হা করে হাসলেন, ‘বাবুর্চি বলতেও পারেন ইচ্ছা করলে। গিনিপিগ বাবুর্চি!’

	‘মানে?’

	‘আমার রান্না করে দেয় গিনিপিগেরা!’

	আমি গরম চা খাচ্ছিলাম, বিষম খেয়ে তালু পুড়ে গেল। মুখ হাঁ করে খানিকক্ষণ বাতাস টেনে জিজ্ঞেস করলাম, ‘কী বললেন! গিনিপিগেরা?’

	‘হুঁ!’ সফদর আলী চোখ নাচিয়ে বললেন, ‘এত অবাক হচ্ছেন কেন, ব্যাপারটা কঠিন কিছু নয়, একটু সময় লাগে আর কী!’

	আমি ঠিক বুঝতে পারছিলাম না, সফদর আলী আমার সাথে ঠাট্টা করছেন কিনা। এমনিতে অবশ্যি তিনি ঠাট্টা-তামাশার মানুষ নন, কিন্তু তাই বলে গিনিপিগ রান্না করছে সেটা বিশ্বাস করি কীভাবে? সফদর আলী আমার অবিশ্বাসের দৃষ্টি দেখে জিজ্ঞেস করলেন, ‘কম্পিউটারে অনেক কঠিন কঠিন সমস্যার সমাধান হয় কি-না?’

	আমি মাথা নাড়লাম, ‘হয়।’

	‘কীভাবে হয়?’

	আমি জানতাম না, তাই চুপ করে থাকলাম। সফদর আলী আমাকে বোঝানোর চেষ্টা করলেন, ‘কম্পিউটার কখনোই একটা কঠিন সমস্যা একবারে করে না। সেটাকে আগে ছোট ছোট অংশে ভাগ করে দেয়া হয়, তাকে বলে প্রোগ্রাম করা। প্রত্যেকটি ছোট ছোট অংশ আলাদা আলাদাভাবে খুব সহজ। কিন্তু সবগুলো একত্র হয়ে একটি জটিল সমস্যার সমাধান হয়। রান্না করার ব্যাপারটাও তাই, পুরো রান্না ব্যাপারটা অনেক কঠিন, কিন্তু রান্না করাকে যদি ছোট ছোট অংশে ভাগ করে ফেলা হয়,তা হলে সেই ছোট অংশগুলো কিন্তু মোটেও কঠিন নয়, যেমন একটা ডেকচি চুলোর উপরে রাখা বা ডেকচিতে খানিকটা ঘি ঢালা বা ঘি-এর মাঝে তেজপাতা ছেড়ে দেয়া—এই ছোট ছোট কাজগুলো যে কেউ করতে পারবে, একটু কষ্ট করে গিনিপিগকে শিখিয়ে দিলে একটা গিনিপিগও করতে পারবে।’

	সফদর আলী থামলেন। আর আমার মনে হল আমি খানিকটা বুঝতে পারছি তিনি কীভাবে গিনিপিগকে দিয়ে রান্না করিয়ে নিচ্ছেন। আমার তখনো পুরোপুরি বিশ্বাস হয় নি, তাই সফদর আলী আবার শুরু করলেন, ‘আমি করেছি কী, অনেকগুলো গিনিপিগকে নিয়ে তাদের প্রত্যেকটিকে আলাদা আলাদা জিনিস করতে শিখিয়েছি। যেমন একটা গিনিপিগ একটা ডেকচি ঠেলে চুলোর ওপরে তুলে দেয়, সে আর কিছুই পারে না। যখনই একটা ঘন্টা বাজে তখনই সে ঠেলে ঠেলে একটা ডেকচি চুলোর উপরে তুলে দেয়। সেটা শেখানো এমন কিছু কঠিন না—দু’দিন এক ঘণ্টা করে শেখাতেই শিখে গেল। এর পরের গিনিপিগটা শুধু চুলোটা জ্বালিয়ে দেয়, সেটা শেখানো একটু কঠিন হয়েছিল, প্রথম প্রথম লোম পুড়ে যেত, এখন সাবধান হয়ে গেছে, আর লোম পোড়ে না। চুলো ধরে যাবার পর তিন নম্বর গিনিপিগটা এসে ডেকচিতে খানিকটা ঘি ঢেলে দেয়, তারপরই তার ছুটি। এরকম সব মিলিয়ে চিচিংগাইশটা গিনিপিগ আছে—’

	আমি বাধা দিয়ে জিজ্ঞেস করলাম, ‘চিচিংগাইশ মানে কি?’

	‘ও, চিচিংগাইশ হচ্ছে দুই আর কিলি, তার মানে ষোল দু’গুণে বত্ৰিশ যোগ কিলি, তার মানে আপনাদের তেতাল্লিশ! যাই হোক এই তেতাল্লিশটা গিনিপিগ তেতাল্লিশটা ভিন্ন ভিন্ন ছোট ছোট কাজ করে, সবগুলো যখন শেষ হয় তখন চমৎকার এক ডেকচি বিরিয়ানি রান্না হয়ে যায়।’ সফদর আলী কথা শেষ করে একটু হাসলেন।